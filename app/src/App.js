import React, { Component } from 'react';
import Button from 'material-ui/RaisedButton';
import './App.css';

import muiThemeable from 'material-ui/styles/muiThemeable';

class App extends Component {
  goTo(route) {
    this.props.history.replace(`/${route}`)
  }

  login() {
    this.props.auth.login();
  }

  logout() {
    this.props.auth.logout();
  }

  render() {
    const { isAuthenticated } = this.props.auth;
    const { palette } = this.props.muiTheme;
    console.log(palette);
    return (
      <div>
        <div>
          <div width='100%' style={{
              backgroundColor: palette.primary1Color, 
              height: '44px',
              width: '100%'
              }} >
            <div style={{ float: 'left' }}>
              <h1 style={{ margin: '1px', color: palette.alternateTextColor }}>geekrep</h1>
            </div>
            <div style={{ overflow: 'hidden' }}>
            </div>
            <div style={{ float:'right', padding: '4px' }}>
              {
                !isAuthenticated() && (
                    <Button
                      primary={true}
                      onClick={this.login.bind(this)}
                      label="Log In"
                    />
                  )
              }
              {
                isAuthenticated() && (
                    <Button
                      primary={true}
                      onClick={this.logout.bind(this)}
                      label="Log Out"
                    />
                  )
              }

            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default muiThemeable()(App);
